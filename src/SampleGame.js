// @flow
import Game from './components/Game';
import Assets, { ASSETS } from './assets';

const PIXI = window.PIXI;

type Props = {
  additionalInfo: {
    background: string
  }
}

export default class SampleGame extends Game<Props> {

  button: PIXI.Sprite;
  buttonUpTexture: any;
  buttonDownTexture: any;

  gameDidMount() {
    // Add additional assets to load which are passed through this.props.additionalInfo
    const thingsToLoad = ASSETS.concat([
      this.props.additionalInfo.background
    ]);
    this.loadAssets(thingsToLoad);
  }

  gameDidLoad(loader: any, resources: any) {
    const bg = new PIXI.Sprite(resources[this.props.additionalInfo.background].texture)
    const bunny = new PIXI.Sprite(resources[Assets.images.bunny].texture);

    // Setup background
    bg.x = 0;
    bg.y = 0;
    bg.width = this.app.renderer.width;
    bg.height = this.app.renderer.height;
    this.app.stage.addChild(bg);

    // Setup the size and position of the bunny
    bunny.width = 300;
    bunny.height = 300;
    bunny.x = this.app.renderer.width / 2;
    bunny.y = this.app.renderer.height / 2;

    // Rotate around the center
    bunny.anchor.x = 0.5;
    bunny.anchor.y = 0.5;

    // Add the bunny to the scene we are building
    this.app.stage.addChild(bunny);

    // Setup and add the button
    this.buttonUpTexture = resources[Assets.textures.button].textures[0];
    this.buttonDownTexture = resources[Assets.textures.button].textures[1];

    this.button = new PIXI.Sprite(this.buttonUpTexture);
    this.button.width = 230;
    this.button.height = 70;
    this.button.x = (this.app.renderer.width - this.button.width) / 2;
    this.button.y = this.app.renderer.height - 100;
    this.button.interactive = true;
    this.button.buttonMode = true;
    this.button
      // Mouse & touch events are normalized into
      // the pointer* events for handling different
      // button events.
      .on('pointerdown', this.onButtonDown.bind(this))
      .on('pointerup', this.onButtonUp.bind(this))
      .on('pointerupoutside', this.onButtonUp.bind(this))

    this.app.stage.addChild(this.button);
    
    // Listen for frame updates
    this.app.ticker.add(() => {
      // each frame we spin the bunny around a bit
      bunny.rotation += 0.01;
    });

  }

  gameDidReset(newProps: Props) {
    // Handle game reset here with new data    
  }

  onButtonDown() {
    this.button.texture = this.buttonDownTexture;
  }

  onButtonUp() {
    this.button.texture = this.buttonUpTexture;
  }
}