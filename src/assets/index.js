//@flow
import background from './images/background.jpg';
import bunny from './images/bunny.png';
import button from './textures/button.json';

/* Define common assets here */
const Assets = {
  images: {
    background: background,
    bunny: bunny
  },
  textures: {
    button: button
  },
  sounds: {
  },
  fonts:{
  }
}

/* Array of common assets to be used by Hexi Loader */
export const ASSETS = [
  Assets.images.background,
  Assets.images.bunny,
  Assets.textures.button
];

export default Assets;
