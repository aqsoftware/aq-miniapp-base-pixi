// @flow
import MiniApp from './components/MiniApp';
import SampleGame from './SampleGame';

const GAME_WIDTH = window.innerWidth;
const GAME_HEIGHT = window.innerHeight;
const DEVT = true;

/**
 * shouldWin - Tells the MiniApp to force the current game iteration to win
 * winImage - Optional. Image URL of item won. Only present if shouldWin is true.
 * source - User info of current user playing the MiniApp
 * engagementSource - User info of user who created the instance of the MiniApp
 * additionalInfo - Data specific to the MiniApp.
 */
let data = {
  shouldWin: false,
  winImage: "https://s3.amazonaws.com/famers/720/F1040881145111POSYEB.png",
  additionalInfo: {
    background: "https://s3.amazonaws.com/famers/720/F1040881145112DFY3HK.jpg"
  }
} 

const miniApp = new MiniApp({
  width: GAME_WIDTH,
  height: GAME_HEIGHT,
  game: SampleGame,
  devt: DEVT,
  data: data
})






